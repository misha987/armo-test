﻿namespace ARMO_Test
{
    partial class formSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formSearch));
            this.textBoxSearchPath = new System.Windows.Forms.TextBox();
            this.labelSearchPath = new System.Windows.Forms.Label();
            this.buttonSelectSearchPath = new System.Windows.Forms.Button();
            this.labelFileName = new System.Windows.Forms.Label();
            this.textBoxFileName = new System.Windows.Forms.TextBox();
            this.labelTextInFile = new System.Windows.Forms.Label();
            this.richTextBoxTextInFile = new System.Windows.Forms.RichTextBox();
            this.buttonFind = new System.Windows.Forms.Button();
            this.buttonPause = new System.Windows.Forms.Button();
            this.buttonContinue = new System.Windows.Forms.Button();
            this.treeViewSearchResults = new System.Windows.Forms.TreeView();
            this.richTextBoxSearchInfo = new System.Windows.Forms.RichTextBox();
            this.labelInfo = new System.Windows.Forms.Label();
            this.labelCountFiles = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.labelCountFilesValue = new System.Windows.Forms.Label();
            this.labelTimeValue = new System.Windows.Forms.Label();
            this.labelResult = new System.Windows.Forms.Label();
            this.labelFinalResult = new System.Windows.Forms.Label();
            this.richTextBoxMsgs = new System.Windows.Forms.RichTextBox();
            this.labelMsgs = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxSearchPath
            // 
            this.textBoxSearchPath.Location = new System.Drawing.Point(99, 6);
            this.textBoxSearchPath.Name = "textBoxSearchPath";
            this.textBoxSearchPath.Size = new System.Drawing.Size(100, 20);
            this.textBoxSearchPath.TabIndex = 0;
            // 
            // labelSearchPath
            // 
            this.labelSearchPath.AutoSize = true;
            this.labelSearchPath.Location = new System.Drawing.Point(12, 9);
            this.labelSearchPath.Name = "labelSearchPath";
            this.labelSearchPath.Size = new System.Drawing.Size(81, 13);
            this.labelSearchPath.TabIndex = 1;
            this.labelSearchPath.Text = "Папка поиска:";
            // 
            // buttonSelectSearchPath
            // 
            this.buttonSelectSearchPath.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonSelectSearchPath.BackgroundImage")));
            this.buttonSelectSearchPath.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonSelectSearchPath.Location = new System.Drawing.Point(205, 6);
            this.buttonSelectSearchPath.Name = "buttonSelectSearchPath";
            this.buttonSelectSearchPath.Size = new System.Drawing.Size(19, 20);
            this.buttonSelectSearchPath.TabIndex = 2;
            this.buttonSelectSearchPath.UseVisualStyleBackColor = true;
            this.buttonSelectSearchPath.Click += new System.EventHandler(this.buttonSelectSearchPath_Click);
            // 
            // labelFileName
            // 
            this.labelFileName.AutoSize = true;
            this.labelFileName.Location = new System.Drawing.Point(241, 9);
            this.labelFileName.Name = "labelFileName";
            this.labelFileName.Size = new System.Drawing.Size(67, 13);
            this.labelFileName.TabIndex = 3;
            this.labelFileName.Text = "Имя файла:";
            // 
            // textBoxFileName
            // 
            this.textBoxFileName.Location = new System.Drawing.Point(307, 7);
            this.textBoxFileName.Name = "textBoxFileName";
            this.textBoxFileName.Size = new System.Drawing.Size(100, 20);
            this.textBoxFileName.TabIndex = 4;
            // 
            // labelTextInFile
            // 
            this.labelTextInFile.AutoSize = true;
            this.labelTextInFile.Location = new System.Drawing.Point(12, 53);
            this.labelTextInFile.Name = "labelTextInFile";
            this.labelTextInFile.Size = new System.Drawing.Size(84, 13);
            this.labelTextInFile.TabIndex = 5;
            this.labelTextInFile.Text = "Текст в файле:";
            // 
            // richTextBoxTextInFile
            // 
            this.richTextBoxTextInFile.Location = new System.Drawing.Point(99, 32);
            this.richTextBoxTextInFile.Name = "richTextBoxTextInFile";
            this.richTextBoxTextInFile.Size = new System.Drawing.Size(189, 81);
            this.richTextBoxTextInFile.TabIndex = 6;
            this.richTextBoxTextInFile.Text = "";
            // 
            // buttonFind
            // 
            this.buttonFind.Location = new System.Drawing.Point(307, 33);
            this.buttonFind.Name = "buttonFind";
            this.buttonFind.Size = new System.Drawing.Size(75, 23);
            this.buttonFind.TabIndex = 7;
            this.buttonFind.Text = "Поиск!";
            this.buttonFind.UseVisualStyleBackColor = true;
            this.buttonFind.Click += new System.EventHandler(this.buttonFind_Click);
            // 
            // buttonPause
            // 
            this.buttonPause.Location = new System.Drawing.Point(307, 62);
            this.buttonPause.Name = "buttonPause";
            this.buttonPause.Size = new System.Drawing.Size(75, 23);
            this.buttonPause.TabIndex = 10;
            this.buttonPause.Text = "Пауза";
            this.buttonPause.UseVisualStyleBackColor = true;
            this.buttonPause.Click += new System.EventHandler(this.buttonPause_Click);
            // 
            // buttonContinue
            // 
            this.buttonContinue.Location = new System.Drawing.Point(307, 91);
            this.buttonContinue.Name = "buttonContinue";
            this.buttonContinue.Size = new System.Drawing.Size(88, 23);
            this.buttonContinue.TabIndex = 11;
            this.buttonContinue.Text = "Продолжить";
            this.buttonContinue.UseVisualStyleBackColor = true;
            this.buttonContinue.Click += new System.EventHandler(this.buttonContinue_Click);
            // 
            // treeViewSearchResults
            // 
            this.treeViewSearchResults.Location = new System.Drawing.Point(760, 32);
            this.treeViewSearchResults.Name = "treeViewSearchResults";
            this.treeViewSearchResults.Size = new System.Drawing.Size(375, 395);
            this.treeViewSearchResults.TabIndex = 12;
            // 
            // richTextBoxSearchInfo
            // 
            this.richTextBoxSearchInfo.Location = new System.Drawing.Point(415, 32);
            this.richTextBoxSearchInfo.Name = "richTextBoxSearchInfo";
            this.richTextBoxSearchInfo.Size = new System.Drawing.Size(339, 333);
            this.richTextBoxSearchInfo.TabIndex = 9;
            this.richTextBoxSearchInfo.Text = "";
            // 
            // labelInfo
            // 
            this.labelInfo.AutoSize = true;
            this.labelInfo.Location = new System.Drawing.Point(554, 16);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(73, 13);
            this.labelInfo.TabIndex = 13;
            this.labelInfo.Text = "Информация";
            // 
            // labelCountFiles
            // 
            this.labelCountFiles.AutoSize = true;
            this.labelCountFiles.Location = new System.Drawing.Point(412, 382);
            this.labelCountFiles.Name = "labelCountFiles";
            this.labelCountFiles.Size = new System.Drawing.Size(160, 13);
            this.labelCountFiles.TabIndex = 14;
            this.labelCountFiles.Text = "Кол-во обработанных файлов:";
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Location = new System.Drawing.Point(415, 399);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(82, 13);
            this.labelTime.TabIndex = 15;
            this.labelTime.Text = "Время поиска:";
            // 
            // labelCountFilesValue
            // 
            this.labelCountFilesValue.AutoSize = true;
            this.labelCountFilesValue.Location = new System.Drawing.Point(578, 382);
            this.labelCountFilesValue.Name = "labelCountFilesValue";
            this.labelCountFilesValue.Size = new System.Drawing.Size(0, 13);
            this.labelCountFilesValue.TabIndex = 16;
            // 
            // labelTimeValue
            // 
            this.labelTimeValue.AutoSize = true;
            this.labelTimeValue.Location = new System.Drawing.Point(504, 399);
            this.labelTimeValue.Name = "labelTimeValue";
            this.labelTimeValue.Size = new System.Drawing.Size(0, 13);
            this.labelTimeValue.TabIndex = 17;
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Location = new System.Drawing.Point(902, 9);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(59, 13);
            this.labelResult.TabIndex = 18;
            this.labelResult.Text = "Результат";
            // 
            // labelFinalResult
            // 
            this.labelFinalResult.AutoSize = true;
            this.labelFinalResult.Location = new System.Drawing.Point(441, 425);
            this.labelFinalResult.Name = "labelFinalResult";
            this.labelFinalResult.Size = new System.Drawing.Size(0, 13);
            this.labelFinalResult.TabIndex = 19;
            // 
            // richTextBoxMsgs
            // 
            this.richTextBoxMsgs.Location = new System.Drawing.Point(15, 184);
            this.richTextBoxMsgs.Name = "richTextBoxMsgs";
            this.richTextBoxMsgs.Size = new System.Drawing.Size(380, 243);
            this.richTextBoxMsgs.TabIndex = 20;
            this.richTextBoxMsgs.Text = "";
            // 
            // labelMsgs
            // 
            this.labelMsgs.AutoSize = true;
            this.labelMsgs.Location = new System.Drawing.Point(123, 165);
            this.labelMsgs.Name = "labelMsgs";
            this.labelMsgs.Size = new System.Drawing.Size(65, 13);
            this.labelMsgs.TabIndex = 21;
            this.labelMsgs.Text = "Сообщения";
            // 
            // formSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1147, 450);
            this.Controls.Add(this.labelMsgs);
            this.Controls.Add(this.richTextBoxMsgs);
            this.Controls.Add(this.labelFinalResult);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.labelTimeValue);
            this.Controls.Add(this.labelCountFilesValue);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.labelCountFiles);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.treeViewSearchResults);
            this.Controls.Add(this.buttonContinue);
            this.Controls.Add(this.buttonPause);
            this.Controls.Add(this.richTextBoxSearchInfo);
            this.Controls.Add(this.buttonFind);
            this.Controls.Add(this.richTextBoxTextInFile);
            this.Controls.Add(this.labelTextInFile);
            this.Controls.Add(this.textBoxFileName);
            this.Controls.Add(this.labelFileName);
            this.Controls.Add(this.buttonSelectSearchPath);
            this.Controls.Add(this.labelSearchPath);
            this.Controls.Add(this.textBoxSearchPath);
            this.Name = "formSearch";
            this.Text = "MySearchWindow";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormSearch_FormClosed);
            this.Shown += new System.EventHandler(this.FormSearch_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxSearchPath;
        private System.Windows.Forms.Label labelSearchPath;
        private System.Windows.Forms.Button buttonSelectSearchPath;
        private System.Windows.Forms.Label labelFileName;
        private System.Windows.Forms.TextBox textBoxFileName;
        private System.Windows.Forms.Label labelTextInFile;
        private System.Windows.Forms.RichTextBox richTextBoxTextInFile;
        private System.Windows.Forms.Button buttonFind;
        private System.Windows.Forms.Button buttonPause;
        private System.Windows.Forms.Button buttonContinue;
        private System.Windows.Forms.TreeView treeViewSearchResults;
        private System.Windows.Forms.RichTextBox richTextBoxSearchInfo;
        private System.Windows.Forms.Label labelInfo;
        private System.Windows.Forms.Label labelCountFiles;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label labelCountFilesValue;
        private System.Windows.Forms.Label labelTimeValue;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.Label labelFinalResult;
        private System.Windows.Forms.RichTextBox richTextBoxMsgs;
        private System.Windows.Forms.Label labelMsgs;
    }
}

