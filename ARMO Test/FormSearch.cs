﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ARMO_Test
{
    public partial class formSearch : Form
    {
        private Thread searchThread, searchTimeThread;
        private Stopwatch watch;
        private int filesCount;
        private string fileName;
        private string searchText;
        private EventWaitHandle waitHandleTimer = new ManualResetEvent(true);

        public formSearch()
        {
            InitializeComponent();
        }

        private void buttonSelectSearchPath_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog fbd = new FolderBrowserDialog())
            {
                if (fbd.ShowDialog() == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    textBoxSearchPath.Text = fbd.SelectedPath;
                }
            }
        }

        private void buttonFind_Click(object sender, EventArgs e)
        {
            if (!Path.IsPathRooted(textBoxSearchPath.Text))
            {
                MessageBox.Show("Выберите начальную папку");
                return;
            }

            if (searchThread != null)
            {
                if (searchThread.ThreadState == System.Threading.ThreadState.Suspended)
                    searchThread.Resume();

                searchThread.Abort();
            }
            if (searchTimeThread != null)
            {
                searchTimeThread.Abort();
            }

            filesCount = 0;
            richTextBoxSearchInfo.Text = "";
            labelCountFilesValue.Text = "";
            labelTimeValue.Text = "";
            treeViewSearchResults.Nodes.Clear();
            labelFinalResult.Text = "";

            searchThread = new Thread(StartSearching);
            searchTimeThread = new Thread(StartTimer);
            searchThread.Start();
            searchTimeThread.Start();
        }

        private void StartTimer()
        {
            watch = Stopwatch.StartNew();
            while (true)
            {
                TimeSpan ts = TimeSpan.FromMilliseconds(watch.ElapsedMilliseconds);
                string formatTime = string.Format("{0:D2}:{1:D2}:{2:D2}",
                                        ts.Hours,
                                        ts.Minutes,
                                        ts.Seconds);
                UpdateTimerLabel(formatTime);
                waitHandleTimer.WaitOne();
            }
        }

        private void StartSearching()
        {
            string startPath = textBoxSearchPath.Text;
            fileName = textBoxFileName.Text;
            searchText = richTextBoxTextInFile.Text;

            MySearchFile(startPath);
            labelFinalResult.Text = "Поиск окончен";
            if (treeViewSearchResults.Nodes.Count == 0)
            {
                AddNodeToTreeView(null, new TreeNode("Ничего не найдено"));
            }
            searchTimeThread.Abort();
        }

        private bool MySearchFile(string startPath, TreeNode parentNode = null, TreeNode newNodeList = null, TreeNode newNodeHead = null)
        {
            bool find = false;
            string[] subDirs;
            try
            {
                subDirs = Directory.GetDirectories(startPath);
            }
            catch (UnauthorizedAccessException ex)
            {
                richTextBoxMsgs.Text += "Ошибка при обработке папки " + startPath + ". Недостаточно прав доступа\n";
                return false;
            }
            catch (ThreadAbortException ex)
            {
                richTextBoxMsgs.Text += "Неизвестная ошибка прервала выполнение функции\n";
                return false;
            }
            if (newNodeHead == null)
            {
                if (treeViewSearchResults.Nodes.Count == 0)
                    newNodeHead = newNodeList = new TreeNode(startPath);
                else
                {
                    string formatPath = startPath.Replace(Path.GetDirectoryName(startPath) + "\\", "");
                    newNodeHead = newNodeList = new TreeNode(formatPath);
                }
            }
            else
            {
                string formatPath = startPath.Replace(Path.GetDirectoryName(startPath) + "\\", "");
                newNodeList = newNodeList.Nodes.Add(formatPath);
            }

            foreach (string currentDir in subDirs)
            {
                if (MySearchFile(currentDir, parentNode, newNodeList, newNodeHead))
                {
                    find = true;
                    parentNode = newNodeList.LastNode.Parent;
                    newNodeHead = null;
                }
            }

            string[] folderFiles;
            try
            {
                folderFiles = Directory.GetFiles(startPath, "*.txt");
            }
            catch (UnauthorizedAccessException ex)
            {
                richTextBoxMsgs.Text += "Ошибка при обработке папки " + startPath + ". Недостаточно прав доступа\n";
                return false;
            }
            catch (ThreadAbortException ex)
            {
                richTextBoxMsgs.Text += "Неизвестная ошибка прервала выполнение функции\n";
                return false;
            }
            foreach (string currentFile in folderFiles)
            {
                richTextBoxSearchInfo.Text += "Обрабатывается файл " + currentFile + '\n';
                if (currentFile.IndexOf(fileName) != -1)
                {
                    find = true;
                    CheckAddNode(parentNode, newNodeList, newNodeHead, Path.GetFileName(currentFile));
                    richTextBoxSearchInfo.Text += "Файл " + currentFile + " обработан" + '\n';
                    labelCountFilesValue.Text = (++filesCount).ToString();
                    continue;
                }

                if (searchText != "")
                {
                    using (StreamReader fstream = new StreamReader(currentFile))
                    {
                        string prevTextBlock = "";
                        char[] readSymbols = new char[searchText.Length];
                        while (!fstream.EndOfStream)
                        {
                            fstream.Read(readSymbols, 0, searchText.Length);
                            string currentBlock = new string(readSymbols);
                            if ((prevTextBlock + currentBlock).IndexOf(searchText) != -1)
                            {
                                find = true;
                                CheckAddNode(parentNode, newNodeList, newNodeHead, Path.GetFileName(currentFile));
                                break;
                            }
                            prevTextBlock = currentBlock;
                        }
                    }
                }
                richTextBoxSearchInfo.Text += "Файл " + currentFile + " обработан" + '\n';
                labelCountFilesValue.Text = (++filesCount).ToString();
            }
            if (!find)
            {
                TreeNode tmp = newNodeList.Parent;
                newNodeList.Remove();
                newNodeList = tmp;
            }
            return find;
        }

        private void FormSearch_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (searchThread != null)
            {
                if (searchThread.ThreadState == System.Threading.ThreadState.Suspended)
                    searchThread.Resume();

                searchThread.Abort();
            }
            if (searchTimeThread != null)
                searchTimeThread.Abort();

            using (BinaryWriter writer = new BinaryWriter(File.Open(Environment.CurrentDirectory + "/saveParams.dat", FileMode.Create)))
            {
                byte[] startFolderAsBytes = Encoding.ASCII.GetBytes(textBoxSearchPath.Text);
                byte[] fileNameAsBytes = Encoding.ASCII.GetBytes(textBoxFileName.Text);
                byte[] textInFileAsBytes = Encoding.ASCII.GetBytes(richTextBoxTextInFile.Text);
                writer.Write(textBoxSearchPath.Text);
                writer.Write(textBoxFileName.Text);
                writer.Write(richTextBoxTextInFile.Text);
            }
        }

        private void FormSearch_Shown(object sender, EventArgs e)
        {
            if (!File.Exists(Environment.CurrentDirectory + "/saveParams.dat"))
                return;

            using (BinaryReader reader = new BinaryReader(File.Open(Environment.CurrentDirectory + "/saveParams.dat", FileMode.Open)))
            {
                try
                {
                    textBoxSearchPath.Text = reader.ReadString();
                    textBoxFileName.Text = reader.ReadString();
                    richTextBoxTextInFile.Text = reader.ReadString();
                }
                catch (Exception)
                {
                    MessageBox.Show("Ошибка чтения файла с параметрами");
                }
            }
        }

        private void buttonPause_Click(object sender, EventArgs e)
        {
            if (searchThread != null && searchThread.IsAlive &&
                searchThread.ThreadState != System.Threading.ThreadState.Suspended)
                searchThread.Suspend();
            if (searchTimeThread != null && searchTimeThread.IsAlive)
            {
                waitHandleTimer.Reset();
                watch.Stop();
            }
        }

        private void buttonContinue_Click(object sender, EventArgs e)
        {
            if (searchThread != null && searchThread.ThreadState == System.Threading.ThreadState.Suspended)
                searchThread.Resume();
            if (searchTimeThread != null)
            {
                waitHandleTimer.Set();
                watch.Start();
            }
        }

        public delegate void AddNodeToTreeViewDelegate(TreeNode curCol, TreeNode value);
        private void AddNodeToTreeView(TreeNode curCol, TreeNode value)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new AddNodeToTreeViewDelegate(AddNodeToTreeView), curCol, value);
            }
            else
            {
                if (curCol == null)
                {
                    treeViewSearchResults.Nodes.Add(value);
                }
                else
                    curCol.Nodes.Add(value);
            }
        }

        public delegate void UpdateTimerLabelDelegate(string value);
        private void UpdateTimerLabel(string value)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new UpdateTimerLabelDelegate(UpdateTimerLabel), value);
            }
            else
            {
                labelTimeValue.Text = value;
            }
        }

        private void CheckAddNode(TreeNode parentNode, TreeNode newNodeList, TreeNode newNodeHead, string currentFile)
        {
            if (newNodeHead == null)
            {
                AddNodeToTreeView(parentNode, new TreeNode(currentFile));
            }
            else if (newNodeHead.TreeView == null)
            {
                newNodeList.Nodes.Add(currentFile);
                AddNodeToTreeView(parentNode, newNodeHead);
            }
            else
            {
                AddNodeToTreeView(newNodeList, new TreeNode(currentFile));
            }
        }
    }
}
